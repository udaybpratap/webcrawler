# webcrawler

It's simple Web crawler application based on REST services. It is limited to only single domain. Even though there are external links on the pages but it will not visit those external link. It will list out the all findings of links on single doimains.

Due to time restriction, it has the limited feature only. If time permits, i could have implemented below feature as well:

1. Multithreaded executors to process search which can process all urls in concurrent manner.
2. Generic process API to process specified no of domains.
3. GUI based inputs and processed results.

This application can be execute as standalone as it contain all required dependencies for running environment.


Pre-requisite installed to build: 
--------------
Mavan 3.0 or later
JDK 1.6 or later


Step to build:
--------------------
1. Clone the source code from git hub location on local machine folde using below commad from command promts:

	git clone https://udaybpratap@bitbucket.org/udaybpratap/webcrawler.git

2. Go to the checked-out folder webcrawler and place use below command on command prompt to build:

	mvn clean install

3. After successful build, go to target folder. It will containe the generated jar file of webcrawler application.


Test the result:
--------------------

To test the crawler application, please follow steps:
1. Go to the build target folder and launch the application service using below commands on commands prompt:
	
	java -jar webcrawler.<version no if there>.jar
	
2. Once webcrawler application get started, please open any rest client tester e.g. POSTMAN.

3. Pass the URL as below with GET request methods:

	http://localhost:8080/api/v1/crawler/< domain as prudential.co.uk> e.g http://localhost:8080/api/v1/crawler/prudential.co.uk

it will response the all visited links with links type infomation.


 
