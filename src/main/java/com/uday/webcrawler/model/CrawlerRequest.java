package com.uday.webcrawler.model;

import org.springframework.stereotype.Component;

@Component
public class CrawlerRequest {	
	
	/*@Size(min=1)
	@NotNull
	@Pattern(regexp="(http(s)?://)?([\\w-]+\\.)+[\\w-]+(/[\\w- ;,./?%&=]*)?")*/
	private String domainName;

	public String getDomainName() {
		return domainName;
	}
	
	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}
}
