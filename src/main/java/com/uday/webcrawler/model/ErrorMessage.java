package com.uday.webcrawler.model;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

@Component
public class ErrorMessage {
	private String erorrCode;
	private String errorMsg;
	private HttpStatus httpStatus;
	
	public ErrorMessage() {
		
	}
	
	public ErrorMessage(String erorrCode, String errorMsg, HttpStatus httpStatus) {
		super();
		this.erorrCode = erorrCode;
		this.errorMsg = errorMsg;
		this.httpStatus = httpStatus;
	}
	public String getErorrCode() {
		return erorrCode;
	}
	public void setErorrCode(String erorrCode) {
		this.erorrCode = erorrCode;
	}
	public String getErrorMsg() {
		return errorMsg;
	}
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
	public HttpStatus getHttpStatus() {
		return httpStatus;
	}
	public void setHttpStatus(HttpStatus httpStatus) {
		this.httpStatus = httpStatus;
	}

}
