package com.uday.webcrawler.model;

import java.util.Set;

import org.springframework.stereotype.Component;

@Component
public class CrawlerResponse {

	private Set<String> internalUrls;
	private Set<String> externalUrls;
	private Set<String> imageUrls;
	
	public CrawlerResponse() {
		
	}
		
	public CrawlerResponse(Set<String> internalUrls, Set<String> externalUrls, Set<String> imageUrls) {
		super();
		this.internalUrls = internalUrls;
		this.externalUrls = externalUrls;
		this.imageUrls = imageUrls;
	}
	
	public Set<String> getInternalUrls() {
		return internalUrls;
	}
	public void setInternalUrls(Set<String> internalUrls) {
		this.internalUrls = internalUrls;
	}
	public Set<String> getExternalUrls() {
		return externalUrls;
	}
	public void setExternalUrls(Set<String> externalUrls) {
		this.externalUrls = externalUrls;
	}
	public Set<String> getImageUrls() {
		return imageUrls;
	}
	public void setImageUrls(Set<String> imageUrls) {
		this.imageUrls = imageUrls;
	}
	
}
