package com.uday.webcrawler.service;

import com.uday.webcrawler.exception.CrawlerException;
import com.uday.webcrawler.model.CrawlerResponse;

public interface CrawlerService {
	public CrawlerResponse getCrawleResult(String domainUrl) throws CrawlerException;
}
