package com.uday.webcrawler.service;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uday.webcrawler.exception.CrawlerException;
import com.uday.webcrawler.model.CrawlerResponse;

@Service
public class CrawlerServiceImpl implements CrawlerService{	
	private Set<String> externalUrls;
	private Set<String> internalUrls;
	private Set<String> imageUrls;
	
	private static final int MAX_LINK_VISIT=1000;
	private static final int URL_TYPE_INTERNAL = 1;
	private static final int URL_TYPE_EXTERNAL = 2;
	private static final int URL_TYPE_IMAGE = 3;
	
	@Autowired
	private CrawlerResponse crawlerResponse;
	
	private static final String includePatternString="prudential.co.uk";
	private static final String excludePatternString="pdf|google|twitter";
	private Pattern includePattern;
	private Matcher matcher;
	
	public CrawlerServiceImpl() {
		externalUrls= new HashSet<String>();
		internalUrls= new HashSet<String>();
		imageUrls= new HashSet<String>();
		includePattern = Pattern.compile(includePatternString);		
	}
	
	@Override
	public CrawlerResponse getCrawleResult(String domainUrl) throws CrawlerException {
		
		matcher = includePattern.matcher(domainUrl);
		if(matcher.find()) {
			externalUrls.clear();
			internalUrls.clear();
			imageUrls.clear();
			
			addUrl(domainUrl, URL_TYPE_INTERNAL);
			processPageLinks(domainUrl);
		}
		
		/*System.out.println("All links:");
		for(String s : internalUrls) {
			System.out.println("internal urls: "+s);
		}
		for(String s : externalUrls) {
			System.out.println("external urls: "+s);
		}
		for(String s : imageUrls) {
			System.out.println("Image urls: "+s);
		}*/
		
		crawlerResponse.setExternalUrls(externalUrls);
		crawlerResponse.setInternalUrls(internalUrls);
		crawlerResponse.setImageUrls(imageUrls);
		return crawlerResponse;		
	}
	
	private void processPageLinks(String url) throws CrawlerException{
		Document doc = null;
		Elements pageLinks = null;
		Elements imageLinks = null;
		String absPageUrl= null;
		
		if(!(url.isEmpty() || url.length() < 1)) {			
		
			//If link not already visited
			if(!(externalUrls.contains(url) && internalUrls.contains(url) && imageUrls.contains(url))) {
				try {
								
					System.out.println("Visited url: "+url);				
					
					if(!(url.matches(excludePatternString)) && canVisit(url)) {						
						if((doc = Jsoup.connect(url).ignoreContentType(true).get()) != null) {						
							pageLinks = doc.select("a[href]");					
						
							for(Element pageLink: pageLinks) {
								absPageUrl = pageLink.absUrl("abs:href");
								matcher = includePattern.matcher(absPageUrl);
								if(matcher.find()) {
									if(addUrl(absPageUrl, URL_TYPE_INTERNAL)) {											
										processPageLinks(absPageUrl);
									}
									
								}else {
									if(!absPageUrl.isEmpty()) {
										addUrl(absPageUrl, URL_TYPE_EXTERNAL);
									}								
								}
							}
							
							imageLinks = doc.select("img[src]");
							for(Element imageLink: imageLinks) {
								if(imageLink.attr("src").contains("/Images")||imageLink.attr("src").contains("/images")) {
									addUrl(imageLink.absUrl("abs:src"), URL_TYPE_IMAGE); // imageUrls.add(imageLink.absUrl("abs:src"));
								}
							}						
						}						
						
					}else {
						addUrl(url, URL_TYPE_EXTERNAL);
					}
					
				}catch(IOException ex) {
					throw new CrawlerException("400",ex.getLocalizedMessage());
				}
			}	
		}
	}
	
	private boolean addUrl(String url, int urlType) {
		if(urlType == URL_TYPE_INTERNAL && getSize() < MAX_LINK_VISIT) {
			return internalUrls.add(url);
		}else if(urlType == URL_TYPE_EXTERNAL && getSize() < MAX_LINK_VISIT) {
			return externalUrls.add(url);
		}else if(urlType == URL_TYPE_IMAGE && getSize() < MAX_LINK_VISIT) {
			return imageUrls.add(url);
		}
		return false;
	}
	
	private boolean canVisit(String url) {
		boolean status = false;
		if(url.contains("http") &&
		   url.toLowerCase().indexOf(".pdf") == -1 &&
		   url.toLowerCase().indexOf(".zip") == -1) {
			status = true;
		}
		return status;
	}
	
	private int getSize() {
		return internalUrls.size()+externalUrls.size()+imageUrls.size();
	}
	
	/*public static void main(String[] args) {
		CrawlerService cr = new CrawlerServiceImpl();
		try {
			cr.getCrawleResult("https://www.prudential.co.uk/");
		} catch (CrawlerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			cr = null;
		}
	}*/

}
