package com.uday.webcrawler.exception;

import org.springframework.stereotype.Component;

@Component
public class CrawlerException extends Exception{
	private static final long serialVersionUID = 1L;

	private String errorCode;
	private String errorMsg;
	
	public CrawlerException() {
		
	}
	
	public CrawlerException(String errCode, String msg) {
		super(msg);
		this.errorCode = errCode;
		this.errorMsg = msg;		
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}	
}
