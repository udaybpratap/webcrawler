package com.uday.webcrawler.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.HttpHeadResponseDecorator;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.uday.webcrawler.exception.CrawlerException;
import com.uday.webcrawler.model.ErrorMessage;

@RestControllerAdvice
public class CrawlerExceptionController {

	@Autowired
	private ErrorMessage errorMsg;
	
	@ExceptionHandler(CrawlerException.class)
	public ResponseEntity<ErrorMessage> handleCrawlerException(CrawlerException ex) {		
		return buildResponseEntity(ex);		
	}
	
	@ExceptionHandler(Exception.class)	
	public ResponseEntity<ErrorMessage> handleException(Exception ex) {
		errorMsg.setErorrCode("501");
		errorMsg.setErrorMsg(ex.getLocalizedMessage());
		errorMsg.setHttpStatus(HttpStatus.INTERNAL_SERVER_ERROR);
		return new ResponseEntity<>(errorMsg, errorMsg.getHttpStatus());
	}
	
	private ResponseEntity<ErrorMessage> buildResponseEntity(CrawlerException ex){
		errorMsg.setErorrCode(ex.getErrorCode());
		errorMsg.setErrorMsg(ex.getLocalizedMessage());
		errorMsg.setHttpStatus(HttpStatus.BAD_REQUEST);
		return new ResponseEntity<>(errorMsg, errorMsg.getHttpStatus());
	}
}
