package com.uday.webcrawler.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.uday.webcrawler.exception.CrawlerException;
import com.uday.webcrawler.model.CrawlerResponse;
import com.uday.webcrawler.service.CrawlerService;

@RestController
public class CrawlerController {
	
	@Autowired
	private CrawlerService crawlerService;
	
	@GetMapping("/api/v1/crawler/{domainUrl}")
	public ResponseEntity<Object> getCrwaledResults(@PathVariable String domainUrl) throws CrawlerException {		
		CrawlerResponse response = null;
		if(domainUrl.isEmpty()) {
			throw new CrawlerException("401","Invalid url "+domainUrl);
		}
		
		//Hard coded support for single domain only
		if(!domainUrl.equalsIgnoreCase("prudential.co.uk")) {
			throw new CrawlerException("401","Domain url should be prudential.co.uk ");
		}
		
		if(!(domainUrl.contains("://") && domainUrl.contains("www"))) {
			domainUrl = "https://www."+domainUrl;
		}else if(!domainUrl.contains("://")){
			domainUrl = "https://"+domainUrl;
		}
		
		try {
			response = crawlerService.getCrawleResult(domainUrl);
		}catch(CrawlerException ex) {
			throw ex;
		}		
		
		return new ResponseEntity<>(response, HttpStatus.OK);		
	}
	
	@GetMapping(value= {"/","/api/{v}/crawler"})
	public String getHomePage() {
		return "Welcome to Smart web crawler"; 
	}
}
