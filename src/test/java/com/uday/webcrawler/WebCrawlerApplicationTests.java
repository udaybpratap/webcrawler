package com.uday.webcrawler;

import static org.mockito.Mockito.*;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.uday.webcrawler.exception.CrawlerException;
import com.uday.webcrawler.model.CrawlerResponse;
import com.uday.webcrawler.service.CrawlerService;
import com.uday.webcrawler.service.CrawlerServiceImpl;

@RunWith(MockitoJUnitRunner.Silent.class)
public class WebCrawlerApplicationTests {
	
	@Mock
	CrawlerService crawlerService;
	
	@Mock
	CrawlerResponse response;
	
	@Test
	public void contextLoads() {
	}
	
	@Test
	public void testCrawlerResult() {
		Set<String> result = new HashSet<>();		
		result.add("https://www.prudential.co.uk");		 
		response.setInternalUrls(result);
		
		try {
			when(crawlerService.getCrawleResult("test")).thenReturn(
					response = new CrawlerResponse(result, null, null));
			assertNotNull(response);
			assertTrue(response.getInternalUrls().size() > 0);
			
		} catch (CrawlerException e) {
			e.printStackTrace();
		}
	}	

}
